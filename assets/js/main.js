$(document).ready(function() {
    // topiatube video goedkeuren
    $(".approve-link").on('click', function (event) {
         event.preventDefault();
        var item = $(this).closest('.video-item');
        item.fadeOut(500, function(){
           item.prependTo(".approved table");
           item.fadeIn( 500, null);
        });
    });

    // topiatube video afkeuren
   $(".disapprove-link").on('click', function (event) {
         event.preventDefault();
        var item = $(this).closest('.video-item');
        item.fadeOut(500, function(){
           item.prependTo(".disapproved table");
           item.fadeIn( 500, null);
        });
    });

    // groepsactie, vriendenactie en particulier klik en verwijder item
    $(".remove-item").on('click', function (event) {
         event.preventDefault();
        var item = $(this).closest('.item');
        item.fadeOut(500, function(){
           item.remove();
        });
    });

    // groepsactie, vriendenactie en particulier klik en markeer item
    $(".mark-item").on("click", function(event) {
        event.preventDefault();
        $(this).closest('.panel').toggleClass('panel-primary panel-default');
        if ($(this).closest('.panel').hasClass('panel-primary')) {
            $(this).html('<span class="glyphicon glyphicon-remove"></span> Deselecteer');
        }
        else {
            $(this).html('<span class="glyphicon glyphicon-pencil"></span> Selecteer');
        }
    });


    // groepsactie, vriendenactie en particulier klik en markeer alle items
    var selected = false;
    $(".select-all").on("click", function(event) {
        event.preventDefault();
        $('div.badge-success').each(function() {
            if(selected) {
                $(this).addClass("panel-default").removeClass("panel-primary");
                $(this).find('.mark-item').html('<span class="glyphicon glyphicon-pencil"></span> Selecteer');
                $('.select-all').html('<span class="glyphicon glyphicon-pencil"></span> Selecteer alles');
            }
            else {
                $(this).addClass("panel-primary").removeClass("panel-default");
                $(this).find('.mark-item').html('<span class="glyphicon glyphicon-remove"></span> Deselecteer');
                $('.select-all').html('<span class="glyphicon glyphicon-remove"></span> Deselecteer alles');
            }
        });

        selected = !selected;
    });


    // alle datum pikkers
    $('#datetimepicker1').datetimepicker({
        format: 'LL'}
     );

    $('#datetimepicker6').datetimepicker({
        format: 'LL'}
    );

    $('#datetimepicker7').datetimepicker({
        useCurrent: false,
        format: 'LL'
    });

    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });

    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });



    // opnieuw tooltip initieren
    $("a.preview-tool-tip").tooltip();

    // opnieuw tabs initieren
    $('#myTabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    })

    // opnieuw dropdown initieren
    $(".dropdown-toggle").dropdown();


    // x-editable opties

    $.fn.editable.defaults.mode = 'inline';
    $('.clickable').editable();
    $('#gender').editable({
        type: 'select',
        showbuttons: false,
        title: 'Kies gender',
        placement: 'right',
        value: 2,
        source: [
            {value: 1, text: 'Male'},
            {value: 2, text: 'Female'}
        ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });


    $('#user-language').editable({
        type: 'select',
        showbuttons: false,
        title: 'Kies User Language',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'NL'},
            {value: 2, text: 'EN'},
            {value: 3, text: 'DE'},
            {value: 4, text: 'US'},
            {value: 5, text: 'BE'},
            {value: 6, text: 'FR'},
            {value: 7, text: 'AT'},
            {value: 8, text: 'CH'}
    ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });

    $('#user-keyboard').editable({
        type: 'select',
        showbuttons: false,
        title: 'Kies User Keyboard',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'US'},
            {value: 2, text: 'DE'},
            {value: 3, text: 'NL'},
            {value: 4, text: 'FR'},
            {value: 5, text: 'BE'}
        ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });

    $('#user-type').editable({
        type: 'select',
        showbuttons: false,
        title: 'User type',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'Student'},
            {value: 2, text: 'Test user'}
        ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });

    $('#website').editable({
        type: 'select',
        showbuttons: false,
        title: 'User type',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'TypeTopia.NL'},
            {value: 2, text: 'TippTopia.DE'},
            {value: 1, text: 'TypePlanet.BE'},
            {value: 2, text: 'TypeCursus.NL'},
            {value: 2, text: 'TypeTopia.CO.UK'}
        ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });


    $('.yes-no').editable({
        type: 'select',
        showbuttons: false,
        title: 'Emails',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'ja'},
            {value: 2, text: 'nee'}
        ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });


     $('.status').editable({
        type: 'select',
        showbuttons: false,
        title: 'Status',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'Completed'},
            {value: 2, text: 'Canceled'}
        ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });

    $('.list').editable({
        type: 'select',
        showbuttons: false,
        title: 'Lijst',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'Nog niet gekoppeld'},
            {value: 2, text: 'Amazon DE'}
        ]

        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });

    $('.licentie-status').editable({
        type: 'select',
        showbuttons: false,
        title: 'licentie-status',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'Inactief'},
            {value: 2, text: 'Geactiveerd'}
        ]
        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });

    $('.payment').editable({
        type: 'select',
        showbuttons: false,
        title: 'licentie-status',
        placement: 'right',
        value: 1,
        source: [
            {value: 1, text: 'Complete'},
            {value: 2, text: 'Pending'},
            {value: 3, text: 'Cancelled'},
            {value: 4, text: 'Started'},
            {value: 5, text: 'Failed'},
            {value: 6, text: 'Storno'},
            {value: 7, text: 'Refunded'}
        ]
        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });

        $('.output').editable({
            type: 'select',
            showbuttons: false,
            title: 'output',
            placement: 'right',
            value: 1,
            source: [
                {value: 1, text: 'WORD'},
                {value: 2, text: 'EXCELL'},
                {value: 3, text: 'PDF'}
            ]
            /*
            //uncomment these lines to send data on server
            ,pk: 1
            ,url: '/post'
            */
        });

        $('.cursussen').editable({
            type: 'select',
            showbuttons: false,
            title: 'cursussen',
            placement: 'right',
            value: 1,
            source: [
                {value: 1, text: 'TypeTopia'},
                {value: 2, text: 'Typeplanet'},
                {value: 3, text: 'Tipptopia'}
            ]
            /*
            //uncomment these lines to send data on server
            ,pk: 1
            ,url: '/post'
            */
        });

       $('.categorieen-da').editable({
            type: 'select',
            showbuttons: false,
            title: 'Categorieen',
            placement: 'right',
            value: 1,
            source: [
                {value: 1, text: '01. Aanmelden/proeflessen/positief bericht'},
                {value: 2, text: '02. Persoonlijke Instellingen wijzigen'},
                {value: 3, text: '03. Hoe werkt het lessysteem'},
                {value: 4, text: '04. Advies ouders/begeleiders leerproces'},
                {value: 5, text: '05. Advies ouders/begeleiders nav onze mails'},
                {value: 6, text: '06. Resetten/Hervatten/Vakantie/Ziekte'},
                {value: 7, text: '07. Examen'},
                {value: 8, text: '08. Info (school/groeps)acties'},
                {value: 9, text: '09. Dyslexie'},
                {value: 10, text: '10. Financiele vragen/stopverzoek/opmerkingen'},
                {value: 11, text: '11. Tijdelijke oplossingen'},
                {value: 12, text: '12. Spellen (games)'},
                {value: 13, text: '13. Volwassenen'},
                {value: 14, text: '14. TypeTopia.co.uk'},
                {value: 15, text: '15. Kan niet inloggen op TypeTopia/Typeplanet'},
                {value: 16, text: '16. Flashplayer problemen (zwart scherm)/ Windows 8'},
                {value: 17, text: '17. Toetsenbordproblemen (weergave tekens)'},
                {value: 18, text: '18. Firefox/Chrome zwakke/geen verbinding'},
                {value: 19, text: '19. Teksttrainer enterprobleem'},
                {value: 20, text: '20. Geen e-mail'},
                {value: 21, text: '21. Rapporten'},
                {value: 22, text: '22. Geluid'},
                {value: 23, text: '23. Uitvragen/Printscreen/Technische afdeling'},
                {value: 24, text: '24. FireFox / Linux-Ubuntu / Cache legen / Win XP'},
                {value: 25, text: '25. Tickets afsluiten'},
                {value: 26, text: '26. Oude browserversie'}
            ]
            /*
            //uncomment these lines to send data on server
            ,pk: 1
            ,url: '/post'
            */
        });
});


