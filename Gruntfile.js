module.exports = function(grunt) {
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
			concat: {
				dist: {
					src: [
						'assets/js/transition.js',
						'assets/js/alert.js',
						'assets/js/button.js',
						'assets/js/carousel.js',
						'assets/js/collapse.js',
						'assets/js/dropdown.js',
						'assets/js/tooltip.js',
						'assets/js/popover.js',
						'assets/js/scrollspy.js',
						'assets/js/tab.js',
						'assets/js/affix.js',
						'assets/js/holder.js',
						'assets/js/moment.js',
						'assets/js/nl.js',
						'assets/js/bootstrap-datetimepicker.min.js',
						'assets/js/main.js'

					],
					dest: 'js/production.js',
					}
				},
			uglify: {
				build: {
					src: 'js/production.js',
					dest: 'js/production.min.js'
				}
			},
			imagemin: {
				dynamic: {
					files: [{
						expand: true,
						cwd: 'assets/img/',
						src: ['**/*.{png,jpg,gif}'],
						dest: 'img'
					}]
				}
			},
			sass: {
				dynamic: {
					options: {
						sourcemap: 'none',
						style: 'compressed'
					},
					files: [{
						expand: true,
						cwd: 'assets/scss/',
						src: ['main.scss'],
						dest: 'css/',
						ext: '.min.css'
					}]
				}
			},
              bootlint: {
                options: {
                  stoponerror: false,
                  relaxerror: []
                },
                files: ['*.html']
              }

    });

    require('jit-grunt')(grunt);

    grunt.registerTask('default', ['concat', 'newer:imagemin', 'uglify', 'sass', 'bootlint']);

};
